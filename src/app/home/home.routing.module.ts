import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HOME_ROUTING} from './home.routing';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {
    path: HOME_ROUTING.HOME,
    component: HomeComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class HomeRoutingModule { }
