import { NgModule } from '@angular/core';
import {HomeComponent} from './home/home.component';
import {HomeRoutingModule} from './home.routing.module';
import {AuthModule} from '../auth/auth.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    HomeRoutingModule,
    AuthModule
  ],
  providers: [],
  bootstrap: []
})
export class HomeModule { }
