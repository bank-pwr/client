import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BANK_ROUTING} from './bank.routing';
import {TransactionGuard} from './transaction/transaction.guard';
import {HistoryGuard} from './history/history.guard';
import {AcceptGuard} from './accept/accept.guard';
import {WalletsGuard} from './wallets/wallets.guard';

const appRoutes: Routes = [
  {
    path: BANK_ROUTING.DASHBOARD,
    loadChildren: () => import('./dashboard/dashboard.module').then(mod => mod.DashboardModule),
  },
  {
    path: BANK_ROUTING.HISTORY,
    loadChildren: () => import('./history/history.module').then(mod => mod.HistoryModule),
    canActivate: [HistoryGuard]
  },
  {
    path: BANK_ROUTING.TRANSACTION,
    loadChildren: () => import('./transaction/transaction.module').then(mod => mod.TransactionModule),
    canActivate: [TransactionGuard]
  },
  {
    path: BANK_ROUTING.ACCEPT,
    loadChildren: () => import('./accept/accept.module').then(mod => mod.AcceptModule),
    canActivate: [AcceptGuard]
  },
  {
    path: BANK_ROUTING.WALLETS,
    loadChildren: () => import('./wallets/wallets.module').then(mod => mod.WalletsModule),
    canActivate: [WalletsGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class BankRoutingModule { }
