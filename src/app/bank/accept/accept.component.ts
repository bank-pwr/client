import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TransactionService} from '../transaction.service';
import {UserService} from '../../auth/user.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {TransactionsResponse} from '../transactions.response';
import {Transaction} from '../transaction';
import {ApiResponse} from '../../auth/api.response';

@Component({
  selector: 'bank-accept',
  templateUrl: './accept.component.html',
  styleUrls: ['./accept.component.scss']
})
export class AcceptComponent implements OnInit {
  private transactions: Array<Transaction> = [];

  constructor(private spinner: NgxSpinnerService,
              private transactionsService: TransactionService,
              private user: UserService,
              private router: Router) { }

  ngOnInit() {
    this.spinner.show();
    this.transactionsService.getAllTransactions().subscribe((response: TransactionsResponse) => {
      this.transactions = response.transactions;
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
    });
  }

  acceptTransaction(transaction: Transaction) {
    Swal.fire({
      title: 'Czy chcesz potwierdzić transakcję?',
      icon: 'success',
      html:
        'Tej operacji nie można cofnąć',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        'Tak',
      confirmButtonAriaLabel: 'Yes',
      cancelButtonText:
        'Nie',
      cancelButtonAriaLabel: 'No'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.transactionsService.acceptTransaction(transaction.id).subscribe((response: ApiResponse) => {
          if (response.success) {
            transaction.status = 1;
          }
          this.spinner.hide();
        }, error1 => {
          this.spinner.hide();
        });
      }
    })
  }

  rollbackTransaction(transaction: Transaction) {
    Swal.fire({
      title: 'Czy chcesz wycofać transakcję?',
      icon: 'error',
      html:
        'Tej operacji nie można cofnąć',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        'Tak',
      confirmButtonAriaLabel: 'Yes',
      cancelButtonText:
        'Nie',
      cancelButtonAriaLabel: 'No'
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        this.transactionsService.rollbackTransaction(transaction.id).subscribe((response: ApiResponse) => {
          if (response.success) {
            transaction.status = 2;
          }
          this.spinner.hide();
        }, error1 => {
          this.spinner.hide();
        });
      }
    })
  }
}
