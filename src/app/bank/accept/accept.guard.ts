import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from '../../auth/user.service';

@Injectable({
  providedIn: 'root'
})
export class AcceptGuard implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate() {
    if (!this.user.hasPermission('ACCEPT_TRANSACTION')) {
      this.router.navigateByUrl('/bank');
    }
    return this.user.hasPermission('ACCEPT_TRANSACTION');
  }
}
