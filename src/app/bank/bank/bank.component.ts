import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {UserService} from '../../auth/user.service';
import {User} from '../../auth/user';
import {Wallet} from '../../auth/wallet';

@Component({
  selector: 'bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BankComponent implements OnInit {
  private wallet: Wallet;
  constructor(
    private auth: AuthService,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.wallet = this.userService.getWallet();
  }

  logout() {
    this.auth.logoutUser();
    this.router.navigateByUrl('/');
  }
}
