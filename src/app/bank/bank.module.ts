import { NgModule } from '@angular/core';
import {BankRoutingModule} from './bank.routing.module';
import {CommonModule} from '@angular/common';
import {BankComponent} from './bank/bank.component';

@NgModule({
  declarations: [
    BankComponent,
  ],
  imports: [
    CommonModule,
    BankRoutingModule,
  ],
  bootstrap: []
})
export class BankModule { }
