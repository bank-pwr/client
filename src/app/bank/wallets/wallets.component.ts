import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TransactionService} from '../transaction.service';
import {Wallet} from '../../auth/wallet';
import {UserService} from '../../auth/user.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {WalletsResponse} from '../wallets.response';
import {ApiResponse} from '../../auth/api.response';

@Component({
  selector: 'bank-wallets',
  templateUrl: './wallets.component.html',
  styleUrls: ['./wallets.component.scss']
})
export class WalletsComponent implements OnInit {
  private wallets: Array<Wallet> = [];

  constructor(private spinner: NgxSpinnerService,
              private transactions: TransactionService,
              private user: UserService,
              private router: Router) { }

  ngOnInit() {
    this.spinner.show();
    this.transactions.getWallets().subscribe((response: WalletsResponse) => {
      this.wallets = response.wallets;
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
    });
  }

  lockWallet(wallet: Wallet) {
    this.spinner.show();
    this.transactions.lockWallet(wallet.id).subscribe((response: ApiResponse) => {
      wallet.blocked = 1;
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
    });
  }

  unlockWallet(wallet: Wallet) {
    this.spinner.show();
    this.transactions.unlockWallet(wallet.id).subscribe((response: ApiResponse) => {
      wallet.blocked = 0;
      this.spinner.hide();
    }, error1 => {
      this.spinner.hide();
    });
  }
}
