import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from '../../auth/user.service';
import {Wallet} from '../../auth/wallet';

@Injectable({
  providedIn: 'root'
})
export class WalletsGuard implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate() {
    if (! this.user.hasPermission('BLOCK_ACCOUNT')) {
      this.router.navigateByUrl('/bank');
    }
    return this.user.hasPermission('BLOCK_ACCOUNT')
  }
}
