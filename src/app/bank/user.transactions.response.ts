import {ApiResponse} from '../auth/api.response';
import {Transaction} from './transaction';

export interface UserTransactionsResponse extends ApiResponse {
  inbound_transactions: Array<Transaction>;
  outgoing_transactions: Array<Transaction>;
}
