import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {UserTransactionsResponse} from './user.transactions.response';
import {TransactionsResponse} from './transactions.response';
import {ApiResponse} from '../auth/api.response';
import {WalletsResponse} from './wallets.response';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {

  constructor(private http: HttpClient, private router: Router) {}

  public makeTransaction(toWalletNumber: string, amount: number): Observable<any> {
    return this.http.post<any>(`${environment.servers.gateway}/transactions/make`, {
      to_wallet_number: toWalletNumber,
      amount: amount
    });
  }

  public getUserTransactions(): Observable<UserTransactionsResponse> {
    return this.http.get<UserTransactionsResponse>(`${environment.servers.gateway}/transactions/history`);
  }

  public getAllTransactions(): Observable<TransactionsResponse> {
    return this.http.get<TransactionsResponse>(`${environment.servers.gateway}/transactions/all`);
  }

  public acceptTransaction(transactionId: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/transactions/accept`, {
      transaction_id: transactionId
    });
  }

  public rollbackTransaction(transactionId: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/transactions/rollback`, {
      transaction_id: transactionId
    });
  }

  public getWallets(): Observable<WalletsResponse> {
    return this.http.get<WalletsResponse>(`${environment.servers.gateway}/transactions/get-wallets`);
  }

  public lockWallet(walletId: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/transactions/lock-wallet`, {
      wallet_id: walletId
    });
  }

  public unlockWallet(walletId: number): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/transactions/unlock-wallet`, {
      wallet_id: walletId
    });
  }
}
