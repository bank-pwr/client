import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TransactionService} from '../transaction.service';
import {UserService} from '../../auth/user.service';
import {Wallet} from '../../auth/wallet';

@Component({
  selector: 'bank-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private wallet: Wallet;

  constructor(private spinner: NgxSpinnerService,
              private transactions: TransactionService,
              private user: UserService) { }

  ngOnInit() {
    this.wallet = this.user.getWallet();
  }
}
