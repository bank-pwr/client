import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TransactionService} from '../transaction.service';
import {UserTransactionsResponse} from '../user.transactions.response';
import {Transaction} from '../transaction';

@Component({
  selector: 'bank-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  private outgoingTransactions: Array<Transaction> = [];
  private inboundTransactions: Array<Transaction> = [];

  constructor(private spinner: NgxSpinnerService,
              private transactions: TransactionService) { }

  ngOnInit() {
    this.spinner.show();
    this.transactions.getUserTransactions().subscribe((response: UserTransactionsResponse) => {
      this.outgoingTransactions = response.outgoing_transactions;
      this.inboundTransactions = response.inbound_transactions;
      this.spinner.hide();
    }, error1 => {
      console.log(error1);
      this.spinner.hide();
    });
  }
}
