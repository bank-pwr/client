import { NgModule } from '@angular/core';
import {HistoryComponent} from './history.component';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: HistoryComponent
  },
];

@NgModule({
  declarations: [
    HistoryComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule.forChild(
      routes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: []
})
export class HistoryModule { }
