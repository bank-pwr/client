import {ApiResponse} from '../auth/api.response';
import {Wallet} from '../auth/wallet';

export interface WalletsResponse extends ApiResponse {
  wallets: Array<Wallet>;
}
