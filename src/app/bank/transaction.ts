import {Wallet} from '../auth/wallet';

export interface Transaction {
  id: number;
  from_wallet_id: number;
  to_wallet_id: number;
  value: number;
  status: number;
  created_at: string;
  updated_at: string;
  to_wallet: Wallet;
  from_wallet: Wallet;
}
