import {ApiResponse} from '../auth/api.response';
import {Transaction} from './transaction';

export interface TransactionsResponse extends ApiResponse {
  transactions: Array<Transaction>;
}
