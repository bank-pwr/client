import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TransactionService} from '../transaction.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Wallet} from '../../auth/wallet';
import {UserService} from '../../auth/user.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

interface Transaction {
  to_wallet_number: string;
  amount: number;
}
@Component({
  selector: 'bank-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  private transactionForm: FormGroup;
  private wallet: Wallet;
  private error: boolean = false;

  private transactionData: Transaction = {
    to_wallet_number: null,
    amount: null
  };

  constructor(private spinner: NgxSpinnerService,
              private transactions: TransactionService,
              private user: UserService,
              private router: Router) { }

  ngOnInit() {
    this.wallet = this.user.getWallet();
    this.transactionForm = new FormGroup({
      to_wallet_number: new FormControl(this.transactionData.to_wallet_number, [
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(20),
      ]),
      amount: new FormControl(this.transactionData.amount, [
        Validators.required,
        Validators.min(0.01),
        Validators.max(this.wallet.amount)
      ])
    });
  }

  makeTransaction() {
    this.spinner.show();
    this.error = false;
    const form = this.transactionForm.getRawValue();
    this.transactions.makeTransaction(form.to_wallet_number.toString(), form.amount.toString()).subscribe(response => {
      const wallet = this.user.getWallet();
      wallet.amount -= form.amount;
      this.user.saveWallet(wallet);

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      Toast.fire({
        icon: 'success',
        title: '<span style="color: lightgreen;">Przelew został wysłany</span>'
      }).then(() => {
        this.router.navigateByUrl('bank');
      });
      this.spinner.hide();
    }, error1 => {
      this.error = true;
      this.spinner.hide();
    });
  }

  get to_wallet_number() { return this.transactionForm.get('to_wallet_number'); }
  get amount() { return this.transactionForm.get('amount'); }
}
