import { NgModule } from '@angular/core';
import {TransactionComponent} from './transaction.component';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: TransactionComponent
  },
];

@NgModule({
  declarations: [
    TransactionComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(
      routes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: []
})
export class TransactionModule { }
