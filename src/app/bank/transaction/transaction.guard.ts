import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserService} from '../../auth/user.service';
import {Wallet} from '../../auth/wallet';

@Injectable({
  providedIn: 'root'
})
export class TransactionGuard implements CanActivate {

  constructor(private user: UserService, private router: Router) { }

  canActivate() {
    const wallet: Wallet = this.user.getWallet();
    if (! (this.user.hasPermission('MAKE_TRANSACTION') && !wallet.blocked)) {
      this.router.navigateByUrl('/bank');
    }
    return this.user.hasPermission('MAKE_TRANSACTION') && !wallet.blocked;
  }
}
