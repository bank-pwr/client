export const BANK_ROUTING = {
  DASHBOARD: '',
  HISTORY: 'history',
  TRANSACTION: 'transaction',
  ACCEPT: 'accept',
  WALLETS: 'wallets'
};
