import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AuthErrorResponse} from './auth.error.response';
import {User} from './user';
import {UserResponse} from './user.response';
import {Wallet} from './wallet';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly USER = 'USER';
  private readonly ROLES = 'ROLES';
  private readonly PERMISSIONS = 'PERMISSIONS';
  private readonly WALLET = 'WALLET';
  private readonly CODE = 'CODE';

  constructor(private http: HttpClient, private router: Router) {}

  public getUser(): Observable<User|AuthErrorResponse> {
    const user = this.getUserFromStorage();
    if (user) {
      return of(user);
    }
    return this.http.post<any>(`${environment.servers.gateway}/auth/user`, {}).pipe(
      tap((user: UserResponse) => {
        this.saveUser(user.user);
        this.saveRoles(user.roles);
        this.savePermissions(user.permissions);
        this.saveWallet(user.wallet);
        this.saveCode(user.code);
      }),
      map((user: UserResponse) => user.user),
      catchError((error) => {
        throw error.error;
      }));
  }

  public saveUser(user: User): void {
    localStorage.setItem(this.USER, JSON.stringify(user));
  }

  public saveRoles(roles: Array<string>): void {
    localStorage.setItem(this.ROLES, JSON.stringify(roles))
  }

  public saveWallet(wallet: Wallet): void {
    localStorage.setItem(this.WALLET, JSON.stringify(wallet))
  }

  public getWallet(): Wallet|null {
    const wallet = localStorage.getItem(this.WALLET);

    if (wallet) {
      return JSON.parse(wallet);
    }
    return null;
  }

  public saveCode(code: string): void {
    localStorage.setItem(this.CODE, JSON.stringify(code))
  }

  public getGoogleCode(): string|null {
    const code = localStorage.getItem(this.CODE);

    if (code) {
      return JSON.parse(code);
    }
    return null;
  }

  public getUserRoles(): Array<string>|null {
    const roles = localStorage.getItem(this.ROLES);

    if (roles) {
      return JSON.parse(roles);
    }
    return null;
  }

  public savePermissions(permissions: Array<string>): void {
    localStorage.setItem(this.PERMISSIONS, JSON.stringify(permissions))
  }

  public getUserPermissions(): Array<string>|null {
    const permissions = localStorage.getItem(this.PERMISSIONS);

    if (permissions) {
      return JSON.parse(permissions);
    }
    return null;
  }


  public getUserFromStorage(): User|null {
    const user = localStorage.getItem(this.USER);

    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

  public hasRole(role: string): boolean {
    const roles = this.getUserRoles();
    if (roles) {
      const index = roles.indexOf(role);
      return index > -1;
    }
    return false;
  }

  public hasPermission(permission: string): boolean {
    const permissions = this.getUserPermissions();
    if (permissions) {
      const index = permissions.indexOf(permission);
      return index > -1;
    }
    return false;
  }
}
