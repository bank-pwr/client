import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AuthService, TokenResponse} from '../auth.service';
import {CookieService} from 'ngx-cookie-service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserService} from '../user.service';
import {User} from '../user';
import {APP_ROUTING} from '../../app.routing';

interface Register {
  email: string;
  password: string;
  phone: string;
}

@Component({
  selector: 'auth-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  private registerForm: FormGroup;
  private error: boolean = false;
  private registerData :Register = {
    email: null,
    password: null,
    phone: null
  };

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private auth: AuthService,
              private router:Router,
              private cookie: CookieService,
              private spinner: NgxSpinnerService,
              private userService: UserService) { }

  ngOnInit() {

    this.registerForm = new FormGroup({
      email: new FormControl(this.registerData.email, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(this.registerData.password, [
        Validators.required,
        Validators.minLength(8)
      ]),
      phone: new FormControl(this.registerData.phone, [
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9)
      ])
    });
  }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get phone() { return this.registerForm.get('phone'); }

  register() {
    this.spinner.show();
    this.error = false;
    const password = this.registerForm.get(['password']).value;
    this.registerForm.addControl('password_confirmation', new FormControl());
    this.registerForm.patchValue({password_confirmation: password});
    this.auth.register(this.registerForm.getRawValue()).subscribe((response: TokenResponse) => {
      this.userService.getUser().subscribe((user: User) => {
        this.spinner.hide();
        this.router.navigateByUrl(APP_ROUTING.BANK);
      });

    }, error1 => {
      this.error = true;
      this.spinner.hide();
    });
  }
}
