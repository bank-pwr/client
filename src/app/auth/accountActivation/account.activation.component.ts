import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from '../account.service';
import {ApiResponse} from '../api.response';
import {NgxSpinnerService} from 'ngx-spinner';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthService} from '../auth.service';

@Component({
  selector: 'auth-activation',
  templateUrl: './account.activation.component.html',
  styleUrls: ['./account.activation.component.scss']
})
export class AccountActivationComponent implements OnInit {
  private message: string;
  private activationSuccess: boolean;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private account: AccountService,
              private spinner: NgxSpinnerService,
              private auth: AuthService) { }

  ngOnInit() {
    const code = this.route.snapshot.paramMap.get('code');

    if (code === undefined || !code.length) {
      return this.router.navigateByUrl('/');
    }

    this.spinner.show();
    this.account.activate(code).subscribe((response: ApiResponse) => {
      console.log('response', response);
      this.message = response.message;
      this.activationSuccess = response.success;
      this.spinner.hide();
    }, (error: HttpErrorResponse) => {
      const response: ApiResponse = error.error;
      console.log(error);
      this.message = response.message;
      this.activationSuccess = response.success;
      this.spinner.hide();
    });
  }
}
