export const AUTH_ROUTING = {
  ACTIVATE: 'activate/:code',
  PASSWORD_RESET_WITH_TOKEN: 'password/reset/:token',
  PASSWORD_RESET_WITHOUT_TOKEN: 'password/reset',
};
