import {User} from './user';
import {Wallet} from './wallet';

export interface UserResponse {
  success: boolean;
  message: string;
  user: User;
  roles: Array<string>;
  permissions: Array<string>;
  wallet: Wallet;
  code: string;
}
