import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AUTH_ROUTING} from './auth.routing';
import {AccountActivationComponent} from './accountActivation/account.activation.component';
import {ResetPasswordComponent} from './resetPassword/reset.password.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  {
    path: AUTH_ROUTING.ACTIVATE,
    component: AccountActivationComponent,
  },
  {
    path: AUTH_ROUTING.PASSWORD_RESET_WITH_TOKEN,
    component: ResetPasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: AUTH_ROUTING.PASSWORD_RESET_WITHOUT_TOKEN,
    component: ResetPasswordComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AuthRoutingModule { }
