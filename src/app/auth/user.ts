export interface User {
  id: number;
  name: string|null;
  email: string;
  email_verified_at: string|null;
  active: boolean;
  level: number;
  experience: number;
  cash: number;
  vip_cash: number;
  premium_expires_at: string|null;
  created_at: string;
  updated_at: string;
  deleted_at: string|null;
}
