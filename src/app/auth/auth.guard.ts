import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {APP_ROUTING} from '../app.routing';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate([APP_ROUTING.BANK]);
    }
    return !this.authService.isLoggedIn();
  }
}
