export interface Wallet {
  id: number;
  wallet_number: string;
  amount: number;
  blocked: boolean|number;
  created_at: string;
  updated_at: string;
}
