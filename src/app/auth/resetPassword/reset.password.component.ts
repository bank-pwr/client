import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from '../account.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthService} from '../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiResponse} from '../api.response';
import {HttpErrorResponse} from '@angular/common/http';

export interface SendResetPasswordMail {
  email: string;
}

export interface PasswordReset {
  password: string;
  password_confirmation: string;
}


@Component({
  selector: 'auth-reset-password',
  templateUrl: './reset.password.component.html',
  styleUrls: ['./reset.password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  private tokenIsProvided: boolean;
  private sendMailForm: FormGroup;
  private resetPasswordForm: FormGroup;
  private token: string;
  private sendMailData: SendResetPasswordMail = {
    email: null,
  };
  private resetPasswordData: PasswordReset = {
    password: null,
    password_confirmation: null
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private account: AccountService,
              private spinner: NgxSpinnerService,
              private auth: AuthService) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this.tokenIsProvided = !(this.token === undefined || this.token === null || !this.token.length);
    this.sendMailForm = new FormGroup({
      email: new FormControl(this.sendMailData.email, [
        Validators.required,
        Validators.email
      ])
    });

    this.resetPasswordForm = new FormGroup({
      password: new FormControl(this.resetPasswordData.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(60)
      ]),
      password_confirmation: new FormControl(this.resetPasswordData.password_confirmation)
    }, {
      validators: this.checkPasswords
    });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('password').value;
    let confirmPass = group.get('password_confirmation').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  sendResetMail() {
    const formValues = this.sendMailForm.getRawValue();
    this.spinner.show();
    this.account.sendResetPasswordMail(formValues).subscribe((response: ApiResponse) => {
      console.log('response', response);
      this.spinner.hide();
    }, (error: HttpErrorResponse) => {
      const response: ApiResponse = error.error;
      console.log(error);
      this.spinner.hide();
    });
  }

  resetPassword() {
    const formValues = this.resetPasswordForm.getRawValue();
    this.spinner.show();
    this.account.resetPassword(formValues, this.token).subscribe((response: ApiResponse) => {
      console.log('response', response);
      this.spinner.hide();
    }, (error: HttpErrorResponse) => {
      const response: ApiResponse = error.error;
      console.log(error);
      this.spinner.hide();
    });
  }
}
