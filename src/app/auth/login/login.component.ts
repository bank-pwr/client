import { Component, OnInit } from '@angular/core';
import {AuthService, QrResponse, TokenResponse} from '../auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Login} from './login.interface';
import {AuthErrorResponse} from '../auth.error.response';
import {Router} from '@angular/router';
import {APP_ROUTING} from '../../app.routing';
import {NgxSpinnerService} from 'ngx-spinner';
import {User} from '../user';
import {Code} from './code.interface';
import {UserService} from '../user.service';

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;
  private codeForm: FormGroup;
  private error: boolean;
  private errorMessage: string;
  private qrCode: string;

  private loginData: Login = {
    email: null,
    password: null
  };

  private codeData: Code = {
    code: null
  };

  constructor(private auth: AuthService,
              private router: Router,
              private spinner: NgxSpinnerService,
              private userService: UserService) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(this.loginData.email, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(this.loginData.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(60)
      ])
    });

    this.codeForm = new FormGroup({
      code: new FormControl(this.codeData.code, [
        Validators.required,
      ])
    });
  }

  getQr() {
    this.spinner.show();
    this.error = false;
    this.auth.getQrCode(this.loginForm.getRawValue()).subscribe((response: QrResponse) => {
      this.qrCode = 'code';
      this.spinner.hide();
    }, (error: AuthErrorResponse) => {
      this.spinner.hide();
      this.error = true;
      this.errorMessage = error.message;
    });
  }

  login() {
    this.spinner.show();
    this.error = false;
    this.auth.login(this.loginForm.getRawValue().email, this.codeForm.getRawValue().code).subscribe((response: TokenResponse) => {
      this.userService.getUser().subscribe((user: User) => {
        this.spinner.hide();
        this.router.navigateByUrl(APP_ROUTING.BANK);
      });
    }, (error: AuthErrorResponse) => {
      this.spinner.hide();
      this.error = true;
      this.errorMessage = error.message;
    });
  }

  get code() { return this.codeForm.get('code'); }
  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }
}
