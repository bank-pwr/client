import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Login} from './login/login.interface';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map, mapTo, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AuthErrorResponse} from './auth.error.response';
import {User} from './user';
import {UserResponse} from './user.response';

export interface TokenResponse {
  access_token: string;
  refresh_token: string;
  expires_in: number;
  token_type: string;
}

export interface QrResponse {
  success: boolean;
  code: string;
}
@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private readonly ACCESS_TOKEN = 'ACCESS_TOKEN';
  private readonly EXPIRES_ID = 'EXPIRES_IN';
  private readonly USER = 'USER';
  private readonly ROLES = 'ROLES';
  private readonly PERMISSIONS = 'PERMISSIONS';
  private readonly WALLET = 'WALLET';
  private readonly CODE = 'CODE';

  constructor(private http: HttpClient, private router: Router) {}

  public storeTokenResponse(response: TokenResponse): boolean {
    localStorage.setItem(this.ACCESS_TOKEN, response.access_token);
    localStorage.setItem(this.EXPIRES_ID, String(response.expires_in));
    return true;
  }

  public refreshToken(): Observable<TokenResponse|AuthErrorResponse> {
    return this.http.post<any>(`${environment.servers.gateway}/auth/refresh`, {}).pipe(
      tap((response: TokenResponse) => this.storeTokenResponse(response)),
      catchError((error) => {
        throw error.error;
      }));
  }

  public getUser(): Observable<User|AuthErrorResponse> {
    const user = this.getUserFromStorage();
    if (user) {
      return of(user);
    }
    return this.http.post<any>(`${environment.servers.gateway}/auth/user`, {}).pipe(
      tap((user: UserResponse) => {
        this.saveUser(user.user);
      }),
      map((user: UserResponse) => user.user),
      catchError((error) => {
        throw error.error;
      }));
  }

  public saveUser(user: User) {
    localStorage.setItem(this.USER, JSON.stringify(user));
  }

  public getUserFromStorage(): User|null {
    const user = localStorage.getItem(this.USER);

    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

  public getQrCode(login: Login): Observable<QrResponse|AuthErrorResponse> {
    return this.http.post<QrResponse>(`${environment.servers.gateway}/auth/login`, login).pipe(
      catchError((error) => {
        throw error.error;
      }),
    );
  }

  public login(email: string, code: string): Observable<TokenResponse|AuthErrorResponse> {
    return this.http.post<TokenResponse>(`${environment.servers.gateway}/auth/generate-token`, {email: email, code: code}).pipe(
      tap((tokenResponse: TokenResponse) => this.storeTokenResponse(tokenResponse)),
      catchError((error) => {
        throw error.error;
      }),
    );
  }

  public register(data): Observable<TokenResponse> {
    return this.http.post<TokenResponse>(`${environment.servers.gateway}/auth/register`, data).pipe(
      tap((tokenResponse: TokenResponse) => this.storeTokenResponse(tokenResponse)),
      catchError((error) => {
        throw error.error;
      }),
    );
  }

  public getAccessToken() {
    return localStorage.getItem(this.ACCESS_TOKEN);
  }

  public logoutUser() {
    localStorage.removeItem(this.ACCESS_TOKEN);
    localStorage.removeItem(this.EXPIRES_ID);
    localStorage.removeItem(this.USER);
    localStorage.removeItem(this.ROLES);
    localStorage.removeItem(this.PERMISSIONS);
    localStorage.removeItem(this.WALLET);
    localStorage.removeItem(this.CODE);
  }

  public isLoggedIn(): boolean {
    return !!localStorage.getItem(this.ACCESS_TOKEN);
  }
}
