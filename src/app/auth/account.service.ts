import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {ApiResponse} from './api.response';
import {PasswordReset, SendResetPasswordMail} from './resetPassword/reset.password.component';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private http: HttpClient, private router: Router) {}

  public activate(code: string): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/auth/activate/${code}`, {});
  }

  public sendResetPasswordMail(payload: SendResetPasswordMail): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/auth/password/send-reset`, payload);
  }

  public resetPassword(payload: PasswordReset, token: string): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${environment.servers.gateway}/auth/password/reset`, {...payload, ... {'token': token}});
  }
}
