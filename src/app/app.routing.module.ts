import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {APP_ROUTING} from './app.routing';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AuthGuard} from './auth/auth.guard';
import {BankGuard} from './bank/bank.guard';
import {BankComponent} from './bank/bank/bank.component';

const appRoutes: Routes = [
  {
    path: APP_ROUTING.BANK,
    canActivate: [BankGuard],
    component: BankComponent,
    loadChildren: () => import('./bank/bank.module').then( mod => mod.BankModule)
  },
  {
    path: APP_ROUTING.AUTH,
    loadChildren: () => import('./auth/auth.module').then( mod => mod.AuthModule)
  },
  {
    path: APP_ROUTING.HOME,
    canActivate: [AuthGuard],
    loadChildren: () => import('./home/home.module').then( mod => mod.HomeModule)
  },
  {
    path: APP_ROUTING.PAGE_NOT_FOUND,
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
