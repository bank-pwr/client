import {Component, Injector, OnInit} from '@angular/core';
import {TranslatorService} from './translator/translator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(protected injector: Injector, private translator: TranslatorService) {
    window['injector'] = injector;
  }

  ngOnInit(): void {
    this.translator.initTranslations();
  }
}
