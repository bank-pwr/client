export const APP_ROUTING = {
  BANK: 'bank',
  AUTH: 'auth',
  HOME: '',
  PAGE_NOT_FOUND: '**',
};
